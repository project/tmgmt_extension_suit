<?php

namespace Drupal\tmgmt_extension_suit\Utils;

/**
 * Class FlowScheduler.
 *
 * Schedules items for upload/download.
 *
 * @package Drupal\tmgmt_extension_suit\Utils
 */
class FlowScheduler {

  /**
   * @var UniqueQueueItem
   */
  protected $uniqueQueueItem;

  public function __construct(UniqueQueueItem $unique_queue_item) {
    $this->uniqueQueueItem = $unique_queue_item;
  }

  public function scheduleDownload($job_id, $job_item_id = NULL, $file_id = NULL, $force = FALSE) {
    $this->uniqueQueueItem->addItem(
      'tmgmt_extension_suit_download',
      [
        'tjid' => $job_id,
        'tjiid' => $job_item_id,
        'fid' => $file_id,
      ],
      $force
    );
  }

  public function scheduleUpload($job_id, $data = [], $force = FALSE) {
    $this->uniqueQueueItem->addItem(
      'tmgmt_extension_suit_upload',
      ['tjid' => $job_id] + $data,
      $force
    );
  }

}
