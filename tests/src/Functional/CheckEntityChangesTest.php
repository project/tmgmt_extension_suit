<?php

namespace Drupal\Tests\tmgmt_extension_suit\Functional;

use Drupal\locale\SourceString;

/**
 * Class CheckEntityChangesTest.
 *
 * @group tmgmt_extension_suit
 */
class CheckEntityChangesTest extends TmgmtExtensionSuitTestBase {
  /**
   * Test "Track changes of the translatable entities" feature is turned on:
   * Editing translation does not trigger track entity changes feature.
   */
  public function testTrackTranslatableEntityTranslationChanges() {
    // Create user interface string translation for "Title" field label.
    $source_string = 'Title';
    $storage = \Drupal::service('locale.storage');
    $string = $storage->findString(['source' => $source_string]);

    if (is_null($string)) {
      $string = new SourceString();
      $string->setString($source_string);
      $string->setStorage($storage);
      $string->save();
    }

    $storage->createTranslation([
      'lid' => $string->lid,
      'language' => 'fr',
      'translation' => $source_string . ' FR',
    ])->save();

    // Create fr translation manually.
    $this->drupalGet("fr/node/1/translations/add/en/fr");
    $this->submitForm([
      'title[0][value]' => 'New node test title FR',
    ], $this->t('Save (this translation)')->render());

    $this->requestTranslation([1], 'fr', 1);

    // Submit source edit form without changes.
    $this->drupalGet("node/1/edit");
    $this->submitForm([], $this->t('Save')->render());

    // Submit fr edit form without changes.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $oldHash = $this->getNodeHash(1, 1);
    $this->drupalGet("fr/node/1/edit");
    $this->submitForm([], $this->t('Save (this translation)')->render());
    $newHash = $this->getNodeHash(1, 1);
    $this->assertEquals($oldHash, $newHash);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 0);
  }

  /**
   * Test "Track changes of the translatable entities" feature is turned on.
   */
  public function testTrackTranslatableEntityChanges() {
    $this->requestTranslation([1], 'fr', 1);

    // Submit node edit form without changes.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $oldHash = $this->getNodeHash(1, 1);
    $this->drupalGet("node/1/edit");
    $this->submitForm([], $this->t('Save')->render());
    $newHash = $this->getNodeHash(1, 1);
    $this->assertEquals($oldHash, $newHash);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 0);

    // Submit node edit form with updated title.
    // Expectations:
    // 1. Hash is changed.
    // 2. Job is added to upload queue.
    $this->drupalGet("node/1/edit");
    $this->submitForm([
      'title[0][value]' => 'New node test title',
    ], $this->t('Save')->render());
    $newHash = $this->getNodeHash(1, 1);
    $this->assertNotEquals($oldHash, $newHash);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 1);
  }

  /**
   * Test "Track changes of the translatable entities" feature is turned on.
   *
   * Works only for plugins which implement ExtendedTranslatorPluginInterface
   * interface.
   */
  public function testTrackTranslatableEntityChangesWorksOnlyForExtendedPlugins() {
    $this->requestTranslation([1], 'fr', 1, 'local');

    // Submit node edit form without changes.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $oldHash = $this->getNodeHash(1, 1);
    $this->drupalGet("node/1/edit");
    $this->submitForm([], $this->t('Save')->render());
    $newHash = $this->getNodeHash(1, 1);
    $this->assertEquals($oldHash, $newHash);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 0);

    // Submit node edit form with updated title.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $this->drupalGet("node/1/edit");
    $this->submitForm([
      'title[0][value]' => 'New node test title',
    ], $this->t('Save')->render());
    $newHash = $this->getNodeHash(1, 1);
    $this->assertEquals($oldHash, $newHash);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 0);
  }

  /**
   * Test "Track changes of the translatable entities" feature is turned off.
   */
  public function testDoNotTrackTranslatableEntityChanges() {
    $this->requestTranslation([1], 'fr', 1);

    // Disable tracking feature.
    $this->drupalGet('admin/tmgmt/extension-settings');
    $this->submitForm([
      'do_track_changes' => FALSE,
    ], $this->t('Save configuration')->render());

    // Submit node edit form without changes.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $oldHash = $this->getNodeHash(1, 1);
    $this->drupalGet("node/1/edit");
    $this->submitForm([], $this->t('Save')->render());
    $newHash = $this->getNodeHash(1, 1);
    $this->assertEquals($oldHash, $newHash);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 0);

    // Submit node edit form with updated title.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $this->drupalGet("node/1/edit");
    $this->submitForm([
      'title[0][value]' => 'New node test title',
    ], $this->t('Save')->render());
    $newHash = $this->getNodeHash(1, 1);
    $this->assertEquals($oldHash, $newHash);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 0);
  }

  /**
   * Test "Track changes of the translatable entities" feature is turned on.
   *
   * But languages are not selected.
   */
  public function testDoNotTrackTranslatableEntityChangesIfNoLanguagesSelected() {
    $this->requestTranslation([1], 'fr', 1);

    // Disable tracking feature.
    $this->drupalGet('admin/tmgmt/extension-settings');
    $this->submitForm([
      'do_track_changes' => TRUE,
      'tes_local_test_de' => 0,
      'tes_local_test_fr' => 0,
    ], $this->t('Save configuration')->render());

    // Submit node edit form without changes.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $oldHash = $this->getNodeHash(1, 1);
    $this->drupalGet("node/1/edit");
    $this->submitForm([], $this->t('Save')->render());
    $newHash = $this->getNodeHash(1, 1);
    $this->assertEquals($oldHash, $newHash);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 0);

    // Submit node edit form with updated title.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $this->drupalGet("node/1/edit");
    $this->submitForm([
      'title[0][value]' => 'New node test title',
    ], $this->t('Save')->render());
    $newHash = $this->getNodeHash(1, 1);
    $this->assertEquals($oldHash, $newHash);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 0);
  }

  /**
   * Test "Track changes of the translatable entities" feature is turned on.
   *
   * All languages selected.
   */
  public function testTrackTranslatableEntityChangesAllLanguagesSelected() {
    $this->requestTranslation([1], 'fr', 1);
    $this->requestTranslation([1], 'de', 2);

    // Submit node edit form without changes.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $oldHash1 = $this->getNodeHash(1, 1);
    $oldHash2 = $this->getNodeHash(1, 2);
    $this->drupalGet("node/1/edit");
    $this->submitForm([], $this->t('Save')->render());
    $newHash1 = $this->getNodeHash(1, 1);
    $newHash2 = $this->getNodeHash(1, 2);
    $this->assertEquals($oldHash1, $newHash1);
    $this->assertEquals($oldHash2, $newHash2);

    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 0);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 2), 0);

    // Submit node edit form with updated title.
    // Expectations:
    // 1. Hash is changed.
    // 2. Jobs are added to upload queue.
    $this->drupalGet("node/1/edit");
    $this->submitForm([
      'title[0][value]' => 'New node test title',
    ], $this->t('Save')->render());
    $newHash1 = $this->getNodeHash(1, 1);
    $newHash2 = $this->getNodeHash(1, 2);
    $this->assertNotEquals($oldHash1, $newHash1);
    $this->assertNotEquals($oldHash2, $newHash2);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 1);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 2), 1);
  }

  /**
   * Test "Track changes of the translatable entities" feature is turned on.
   *
   * Not all languages selected.
   */
  public function testTrackTranslatableEntityChangesNotAllLanguagesSelected() {
    $this->requestTranslation([1], 'fr', 1);
    $this->requestTranslation([1], 'de', 2);

    $this->drupalGet('admin/tmgmt/extension-settings');
    $this->submitForm([
      'do_track_changes' => TRUE,
      'tes_local_test_de' => 0,
      'tes_local_test_fr' => 1,
    ], $this->t('Save configuration')->render());

    // Submit node edit form without changes.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $oldHash1 = $this->getNodeHash(1, 1);
    $oldHash2 = $this->getNodeHash(1, 2);
    $this->drupalGet("node/1/edit");
    $this->submitForm([], $this->t('Save')->render());
    $newHash1 = $this->getNodeHash(1, 1);
    $newHash2 = $this->getNodeHash(1, 2);
    $this->assertEquals($oldHash1, $newHash1);
    $this->assertEquals($oldHash2, $newHash2);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 0);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 2), 0);

    // Submit node edit form with updated title.
    // Expectations:
    // 1. Hash is not changed.
    // 2. Job is not added to upload queue.
    $this->drupalGet("node/1/edit");
    $this->submitForm([
      'title[0][value]' => 'New node test title',
    ], $this->t('Save')->render());
    $newHash1 = $this->getNodeHash(1, 1);
    $newHash2 = $this->getNodeHash(1, 2);
    $this->assertNotEquals($oldHash1, $newHash1);
    $this->assertEquals($oldHash2, $newHash2);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 1), 1);
    $this->assertEquals($this->isItemAddedToQueue('tmgmt_extension_suit_upload', 2), 0);
  }

}
